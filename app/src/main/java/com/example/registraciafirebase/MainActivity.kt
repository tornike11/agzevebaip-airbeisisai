package com.example.registraciafirebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {
    private lateinit var editTextMail:EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPassword2: EditText
    private lateinit var register: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        regListener()

    }
    private fun init(){
        editTextMail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPassword2 = findViewById(R.id.editTextPassword2)
        register = findViewById(R.id.register)

    }
    private fun regListener(){
        register.setOnClickListener{
            val email = editTextMail.text.toString()
            val password = editTextPassword.text.toString()
            val passGameorebis = editTextPassword2.text.toString()
            val passwordPattern: Pattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z]).{9,}")


            if(email.contains("@") == false){
                Toast.makeText(this, "შეიყვანეთ ვალიდური მეილი", Toast.LENGTH_SHORT).show()
            }else if(password.length < 9 || passGameorebis.length < 9){
                Toast.makeText(this, "პაროლი უნდა შედგებოდეს 9 ან მეტი სიმბოლოსგან", Toast.LENGTH_SHORT).show()
            }else if (!(passwordPattern.matcher(password).matches())) {
                Toast.makeText(this, "პაროლი უნდა შეიცავდეს ციფრებსა და ასოებს", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }else if((password == passGameorebis) == false){
                Toast.makeText(this, "შეყვანილი პაროლები ერთმანეთს არ ემთხვევა", Toast.LENGTH_SHORT).show()
            }else if(email.isEmpty() || password.isEmpty()||passGameorebis.isEmpty()){
                Toast.makeText(this, "შეავსეთ ყველა ველი", Toast.LENGTH_SHORT).show()
            }else{ FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener{ task ->
                    if(task.isSuccessful){
                        Toast.makeText(this, "წარმატებით დარეგისტრირდით", Toast.LENGTH_SHORT).show()
                        finish()
                    }else{
                        Toast.makeText(this, "3rror", Toast.LENGTH_SHORT).show()
                    }
                }

            }


        }

    }

}